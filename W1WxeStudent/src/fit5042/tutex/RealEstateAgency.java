package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() throws Exception{
    	propertyRepository.addProperty(new Property(1, "1111 asdff 333", 1, 110, 4100000));
    	propertyRepository.addProperty(new Property(2, "2222 asdff 333", 2, 120, 4200000));
    	propertyRepository.addProperty(new Property(3, "3333 asdff 333", 3, 130, 4300000));
    	propertyRepository.addProperty(new Property(4, "4444 asdff 333", 4, 140, 4400000));
    	propertyRepository.addProperty(new Property(5, "5555 asdff 333", 5, 150, 4500000));
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception{
    	List<Property> l = propertyRepository.getAllProperties();
    	for(Property p : l)
    	{
    		System.out.println(p.getId() +" " + p.getAddress() + " "+p.getNumberOfBedrooms()+"BR(s) " + p.getSize() + "sqm $" + p.getPrice() );
    	}
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() throws Exception{
        System.out.print("enter id:");
        Scanner console = new Scanner(System.in);
        int number = Integer.parseInt(console.nextLine());
        Property p = propertyRepository.searchPropertyById(number);
        System.out.println(p.getId() +" " + p.getAddress() + " "+p.getNumberOfBedrooms()+"BR(s) " + p.getSize() + "sqm $" + p.getPrice());
        
    }
    
    public void run() throws Exception{
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
